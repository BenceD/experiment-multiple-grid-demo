import { Container, createMuiTheme, CssBaseline, Grid, ThemeProvider, useMediaQuery, useTheme, withStyles } from '@material-ui/core';
import React from 'react';
import './App.css';
import './placeholder.css';

const themeConfig = {
  breakpoints: {
    values: {
      xs: 0,
      sm: 576,
      md: 768,
      lg: 1024,
      xl: 1440,
    },
  },
};

const theme = createMuiTheme(themeConfig);

const GlobalCss = withStyles({
  '@global': {
    // SPACING OUTSIDE THE GRID
    '.MuiContainer-root': {
      paddingLeft: "16px",
      paddingRight: "16px",

      [theme.breakpoints.up('sm')]: {
        paddingLeft: "16px",
        paddingRight: "16px",
      },
      [theme.breakpoints.up('md')]: {
        paddingLeft: "32px",
        paddingRight: "32px",
      },
      [theme.breakpoints.up('lg')]: {
        paddingLeft: "40px",
        paddingRight: "40px",
      },
      [theme.breakpoints.up('xl')]: {
        paddingLeft: "56px",
        paddingRight: "56px",
      },
    },
  },
})(() => null);

const useWidth = () => {
  const keys = [...theme.breakpoints.keys].reverse();
  return (
    keys.reduce((output:any, key:any) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const matches = useMediaQuery(theme.breakpoints.only(key));
      return !output && matches ? key : output;
    }, null) || 'xs'
  );
}

const Placeholder = () => (<div className="placeholder"></div>);

const TrtGridContainer = ({children}:any) => {
  const bpXs = useMediaQuery(theme.breakpoints.down('xs'));
  const bpSm = useMediaQuery(theme.breakpoints.down('sm'));
  const spaceSize = bpSm || bpXs ? 1:2; //For the xs and sm resolutions, the gap should be 8px, the rest is 16px

  return (
    <Container maxWidth={false} className="trtContainer">
      <Grid container spacing={spaceSize}>{children}</Grid>
    </Container>
  )};


const App = () => {
  const width = useWidth();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline /> {/* CSS RESET FOR CROSS-BROWSER COMPATIBILITY */}
      <GlobalCss />
      <span>{`Current breakpoint: ${width}`}</span>

      <TrtGridContainer>
        <>
        {[...Array(12)].map(()=>(
          <Grid item xs={1} sm={1} md={1} lg={1}>
            <Placeholder />
          </Grid>
        ))}

        {[...Array(6)].map(()=>(
          <Grid item xs={2} sm={2} md={2} lg={2}>
            <Placeholder />
          </Grid>
        ))}

        {[...Array(4)].map(()=>(
          <Grid item xs={3} sm={3} md={3} lg={3}>
            <Placeholder />
          </Grid>
        ))}

        {[...Array(3)].map(()=>(
          <Grid item xs={4} sm={4} md={4} lg={4}>
            <Placeholder />
          </Grid>
        ))}

        {[...Array(2)].map(()=>(
          <Grid item xs={6} sm={6} md={6} lg={6}>
            <Placeholder />
          </Grid>
        ))}
        </>
      </TrtGridContainer>
    </ThemeProvider>
  );
}

export default App;
